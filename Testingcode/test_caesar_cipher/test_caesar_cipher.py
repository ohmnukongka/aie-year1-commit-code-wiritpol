import unittest
from project.caesar_cipher import caesarCipher

class Caesarcipher(unittest.TestCase):
    def test_give_Hello_World_4(self):
        alpha = "Hello_World!"
        space = 4
        expected_result = "Lipps_Asvph!"
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

    def test_give_Ciphering_26(self):
        alpha = "Ciphering."
        space = 26
        expected_result = "Ciphering."
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

    def test_give_wwwabcxy_87(self):
        alpha = "www.abc.xy"
        space = 87
        expected_result = "fff.jkl.gh"
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

    def test_give_D3q4_0(self):
        alpha = "D3q4"
        space = 0
        expected_result = "D3q4"
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

    def test_give_159357lcfd_98(self):
        alpha = "159357lcfd"
        space = 98
        expected_result = "159357fwzx"
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

    def test_give_middleOutz_2(self):
        alpha = "middle-Outz"
        space = 2
        expected_result = "okffng-Qwvb"
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

    def test_give_random_62(self):
        alpha = "!m-rB`-oN!.W`cLAcVbN/CqSoolII!SImji.!w/`Xu`uZa1TWPRq`uRBtok`xPT`lL-zPTc.BSRIhu..-!.!tcl!-U"
        space = 62
        expected_result = "!w-bL`-yX!.G`mVKmFlX/MaCyyvSS!CSwts.!g/`He`eJk1DGZBa`eBLdyu`hZD`vV-jZDm.LCBSre..-!.!dmv!-E"
        result = caesarCipher(alpha,space)
        self.assertEqual(expected_result, result)

