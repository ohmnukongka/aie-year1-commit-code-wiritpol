import unittest
from project.funnystring import funnyString

class Funnystring(unittest.TestCase):
    def test_give_acxz(self):
        alpha = "acxz"
        expected_result = "Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

    def test_give_ivvkxq(self):
        alpha = "ivvkxq"
        expected_result = "Not Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

    def test_give_ovyvzvptyvpvpxyztlrztsrztztqvrxtxuxq(self):
        alpha = "ovyvzvptyvpvpxyztlrztsrztztqvrxtxuxq"
        expected_result = "Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

    def test_give_fmpszyvqwxrtvpuwqszvyvotmsxsxuvzyvpwzrpmuxqwtswvytytzsnuxuyrpvtysqoutzurqxury(self):
        alpha = "fmpszyvqwxrtvpuwqszvyvotmsxsxuvzyvpwzrpmuxqwtswvytytzsnuxuyrpvtysqoutzurqxury"
        expected_result = "Not Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

    def test_give_djnsyzxszryqworuxpqvqwquvotzsqvoupwvztzupowtqnvpxqyrwutzuys(self):
        alpha = "djnsyzxszryqworuxpqvqwquvotzsqvoupwvztzupowtqnvpxqyrwutzuys"
        expected_result = "Not Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

    def test_give_qvskvvqkkmdjouseikfeurwxjovvjxgbuxkqucceoenczhhhrlknegkpwfmheriyqrqltrojwdmvksedgorlvcztimbodimtbeetuteqypnkkqmrgdccgcwopwctgpmhsrconiglwsuiiibokdrqgngwmjprzcztuehhlmtjeprnwrbjvrbbhiwsqmlrplklqssbenjjdcxbtyjqrjrzqpridfytrkfhcyhhlrrntpqmpycgiugzmyiofiivsoctmkdwctmuiciwzlfjimltmujrcsovgqrrctofocyydiwevbpdozvbvetyyhueynvbilfvogtqpqvksmmhnjjipchnkdqjrnjyzptnwpfohbxbomhcwrjlurzftsnmhxkhogtnubbddzdqlmrkdb(self):
        alpha = "qvskvvqkkmdjouseikfeurwxjovvjxgbuxkqucceoenczhhhrlknegkpwfmheriyqrqltrojwdmvksedgorlvcztimbodimtbeetuteqypnkkqmrgdccgcwopwctgpmhsrconiglwsuiiibokdrqgngwmjprzcztuehhlmtjeprnwrbjvrbbhiwsqmlrplklqssbenjjdcxbtyjqrjrzqpridfytrkfhcyhhlrrntpqmpycgiugzmyiofiivsoctmkdwctmuiciwzlfjimltmujrcsovgqrrctofocyydiwevbpdozvbvetyyhueynvbilfvogtqpqvksmmhnjjipchnkdqjrnjyzptnwpfohbxbomhcwrjlurzftsnmhxkhogtnubbddzdqlmrkdb"
        expected_result = "Not Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

    def test_give_xskqyrzyuzxrxpwqunumoumswqvqstwsxzyumnruztlnszskpuntvptzwoqumqyuzryumpvzrwrvyvyqxytuowqrzswpruxruvwyvpvxzxsxszvyrzwrzrwpvnszwptyquoqruyvqyrszxuzystlqvpsxuvopuxpxprsvqtmqrywyzsuovxuyrwopuoqrzyzsuvwqtmorwzvpqvyqwqyvqpvzwromtqwvuszyzrqoupowryuxvouszywyrqmtqvsrpxpxupovuxspvqltsyzuxzsryqvyurqouqytpwzsnvpwrzrwzryvzsxsxzxvpvywvurxurpwszrqwoutyxqyvyvrwrzvpmuyrzuyqmuqowztpvtnupkszsnltzurnmuyzxswtsqvqwsmuomunuqwpxrxzuyzrksyql(self):
        alpha = "xskqyrzyuzxrxpwqunumoumswqvqstwsxzyumnruztlnszskpuntvptzwoqumqyuzryumpvzrwrvyvyqxytuowqrzswpruxruvwyvpvxzxsxszvyrzwrzrwpvnszwptyquoqruyvqyrszxuzystlqvpsxuvopuxpxprsvqtmqrywyzsuovxuyrwopuoqrzyzsuvwqtmorwzvpqvyqwqyvqpvzwromtqwvuszyzrqoupowryuxvouszywyrqmtqvsrpxpxupovuxspvqltsyzuxzsryqvyurqouqytpwzsnvpwrzrwzryvzsxsxzxvpvywvurxurpwszrqwoutyxqyvyvrwrzvpmuyrzuyqmuqowztpvtnupkszsnltzurnmuyzxswtsqvqwsmuomunuqwpxrxzuyzrksyql"
        expected_result = "Funny"
        result = funnyString(alpha)
        self.assertEqual(expected_result, result)

  