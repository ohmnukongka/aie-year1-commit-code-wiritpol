from project.number import is_prime_list
import unittest
class PrimeListTest(unittest.TestCase):
    def test_give_1_2_3_is_prime(self):
        prime_list = [1, 2, 3]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_4_5_6_is_prime(self):
        prime_list = [4, 5, 6]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_100_200_300_is_prime(self):
        prime_list = [100, 200, 300]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_17_19_23_is_prime(self):
        prime_list = [17, 19, 23]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_1_1_1_is_prime(self):
        prime_list = [1, 1, 1]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_11_19_20_is_prime(self):
        prime_list = [11, 19, 20]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_2_2_2_is_prime(self):
        prime_list = [2, 2, 2]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

