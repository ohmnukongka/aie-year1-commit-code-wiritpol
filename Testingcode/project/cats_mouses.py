def cats_and_mouse(x, y, z):
    diff_a = abs(x - z)
    diff_b = abs(y - z)

    if diff_a > diff_b:
        return "Cat B"

    elif diff_a < diff_b:
        return "Cat A"
    else:
        return "Mouse C"
