def gridChallenge(grid):
    grid = [list(i) for i in grid]
    row = len(grid)
    for i in range (row):
        grid[i].sort()
    column = len(grid[0])
    for i in range(column):
        for j in range(1,row):
            if not grid[j-1][i] <= grid[j][i]:
                return "NO"
    return "YES"