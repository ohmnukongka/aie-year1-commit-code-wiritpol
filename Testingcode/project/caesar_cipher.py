def caesarCipher(s, k):
    word = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    wordbig = word.upper()
    count = 0
    box = []
    for i in s:
        box.append(i)
    for i in box:
        i = str(i)
        if i.isascii() and i.islower():
            x = word.index(i)
            box[count] = word[x+k]
            count +=1        
        elif i.isascii() and i.isupper():
            x = wordbig.index(i)
            box[count] = wordbig[x+k]
            count +=1
        else: 
            count +=1
    
    return("".join(box))