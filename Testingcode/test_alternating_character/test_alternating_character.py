from project.alternating_character import alternatingCharacters
import unittest

class alternatingTest(unittest.TestCase):
    def test_give_AA(self):
        string_list = "AA"
        expected_result = 1
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)

    def test_give_AAA_BBB_ABB(self):
        string_list = "AAA BBB ABB"
        expected_result = 5
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)

    def test_give_AAAAAAAAAA(self):
        string_list = "AAAAAAAAAA"
        expected_result = 9
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)

    def test_give_ABABABAB_BABABA(self):
        string_list = "ABABABAB BABABA"
        expected_result = 0
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)

    def test_give_AAABBBAABB_AABBAABB_ABABABAA(self):
        string_list = "AAABBBAABB AABBAABB ABABABAA"
        expected_result = 11
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)

    def test_give_ABBABBAA(self):
        string_list = "ABBABBAA"
        expected_result = 3
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)

    def test_give_AAAA_BBBBB_ABABABAB_BABABA_AAABBB(self):
        string_list = "AAAA BBBBB ABABABAB BABABA AAABBB"
        expected_result = 11
        result = alternatingCharacters(string_list)        
        self.assertEqual(expected_result, result)
    