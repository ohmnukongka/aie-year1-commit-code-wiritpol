import unittest
from project.grid_challenge import gridChallenge

class Gridchallenge(unittest.TestCase):
    def test_give_eabcd_fghij_olkmn_trpqs_xywuv(self):
        alpha = ["eabcd","fghij","olkmn","trpqs","xywuv"]
        expected_result = "YES"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)
    
    def test_give_kc_iu(self):
        alpha = ["kc","iu"]
        expected_result = "YES"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)

    def test_give_ppp(self):
        alpha = ["ppp","ypp","wyw"]
        expected_result = "YES"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)

    def test_give_l(self):
        alpha = ["l"]
        expected_result = "YES"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)

    def test_give_lyivr(self):
        alpha = ["lyivr","jgfew","uweor","qxwyr","uikjd"]
        expected_result = "NO"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)

    def test_give_hcd(self):
        alpha = ["hcd","awc","shm"]
        expected_result = "NO"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)

    def test_give_uxf(self):
        alpha = ["uxf","vof","hmp"]
        expected_result = "NO"
        result = gridChallenge(alpha)
        self.assertEqual(expected_result, result)