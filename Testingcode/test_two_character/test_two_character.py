import unittest
from project.two_character import  alternate

class Two_character(unittest.TestCase):
    def test_give_beabeefeab(self):
        alpha = "beabeefeab"
        expected_result = 5
        result = alternate(alpha)
        self.assertEqual(expected_result, result)

    def test_give_a(self):
        alpha = "a"
        expected_result = 0
        result = alternate(alpha)
        self.assertEqual(expected_result, result)

    def test_give_ab(self):
        alpha = "ab"
        expected_result = 2
        result = alternate(alpha)
        self.assertEqual(expected_result, result)

    def test_give_aaaaa(self):
        alpha = "aaaaa"
        expected_result = 0
        result = alternate(alpha)
        self.assertEqual(expected_result, result)

    def test_give_random(self):
        alpha = "zxvtsuvazzqsohqvnhqwlcfsdobcggbaomhhvpbhfhstpbbxwwwripixzeqcngvuhalpgzuwonrbgvfpmctcnxarwvbwyoanslcixlmudpixelepyqlpusqgrndcgjumzqgyhpmtzngqkbxgajbmpbxdghmzlimmqfmplhmfpnnawabfvavchimulofnkhbyhkvchqvcniwnowamrsbzldyhekkkskwxrsgprihvsyyvsawqabsgvbbpwrgcrjulrjcdpkotxbkcijtykrqrqjxppanqdxdpewesq"
        expected_result = 0
        result = alternate(alpha)
        self.assertEqual(expected_result, result)

    def test_give_asvkugfiugsalddlasguifgukvsa(self):
        alpha = "asvkugfiugsalddlasguifgukvsa"
        expected_result = 0
        result = alternate(alpha)
        self.assertEqual(expected_result, result)

    def test_give_asdcbsdcagfsdbgdfanfghbsfdab(self):
        alpha = "asdcbsdcagfsdbgdfanfghbsfdab"
        expected_result = 8
        result = alternate(alpha)
        self.assertEqual(expected_result, result)