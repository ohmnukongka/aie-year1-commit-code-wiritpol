import unittest
from project.cats_mouses import cats_and_mouse

class CatsAndMouse(unittest.TestCase):
    def test_give_1_2_3_should_cat_b(self):
        cat_a = 1
        cat_b = 2
        mouse_c = 3
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_55_should_cat_b(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 55
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)


    def test_give_55_50_40_should_cat_b(self):
        cat_a = 55
        cat_b = 50
        mouse_c = 40
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)



    def test_give_20_40_25_should_cat_a(self):
        cat_a = 20
        cat_b = 40
        mouse_c = 25
        expected_result = "Cat A"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)


    def test_give_40_40_25_should_mouse_c(self):
        cat_a = 40
        cat_b = 40
        mouse_c = 25
        expected_result = "Mouse C"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_25_25_25_should_mouse_c(self):
        cat_a = 25
        cat_b = 25
        mouse_c = 25
        expected_result = "Mouse C"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_100_0_50_should_mouse_c(self):
        cat_a = 100
        cat_b = 0
        mouse_c = 50
        expected_result = "Mouse C"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

